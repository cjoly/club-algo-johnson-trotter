open Base;;
open Stdio;;

let gen_dict () =
  let aspell_dump = Unix.open_process_in "aspell -d fr dump master" in
  In_channel.fold_lines ~init:(Set.empty (module String)) aspell_dump
    ~f:(fun word_set new_word -> Set.add word_set new_word );
;;

(* Johnson-Trotter, from https://gist.github.com/MassD/be3f6571967662b9d3c6 *)
type direction = L | R
type 'a elt_num_dir = {
  n: int; (* Numbering *)
  v: 'a; (* Initial value *)
  d: direction;
}

let attach_direction_and_order a = Array.mapi (fun n x -> { n; v = x; d = L}) a

let swap a i j = let tmp = a.(j) in a.(j) <- a.(i); a.(i) <- tmp

let is_movable a i =
  let e = a.(i) in
  match e.d with
  (* Heavy *)
  | L -> if i > 0 && e.n > a.(i-1).n then true else false
  | R -> if i < Array.length a - 1 && e.n > a.(i+1).n then true else false

let move a i =
  let e = a.(i) in
  if is_movable a i then
    match e.d with
    | L -> swap a i (i-1)
    | R -> swap a i (i+1)
  else
    failwith "not movable"

let scan_movable_largest a =
  let rec aux acc i =
    if i >= Array.length a then acc
    else if not (is_movable a i) then aux acc (i+1)
    else
      let e = a.(i) in
      match acc with
      | None -> aux (Some i) (i+1)
      | Some j -> aux (if a.(i).n < a.(j).n then acc else Some i) (i+1)
  in
  aux None 0

let flip = function | L -> R | R -> L

let scan_flip_larger j a =
  Array.iteri (fun i e -> if e.n > j then a.(i) <- { e with d = flip e.d }) a
;;

let permutations_generator ( l : char List.t) =
  let a = Array.of_list l |> attach_direction_and_order in
  let r = ref (Some l) in
  let next () =
    let p = !r in
    (match scan_movable_largest a with
     | None -> r := None
     | Some i ->
       let e = a.(i) in (
         move a i;
         scan_flip_larger e.n a;
         r := Some (Array.map (fun b -> b.v) a |> Array.to_list)));
    p
  in
  next

(* example *)
(* let generator = permutations_generator [1;2;3] *)
(*/JT*)

let find_anagram dict word =
  let generator = permutations_generator (String.to_list word) in
  let rec search = function
      None -> ()
    | Some permutation ->
      let pseudo_word = String.of_char_list permutation in
      Set.mem dict pseudo_word
      |> (function true -> printf "Found: %s\n%!" pseudo_word
                 (* | false ->  printf "tested: %s\n%!" pseudo_word *)
                 | false -> ()
        );
      search (generator ())
  in
  search ( generator ())
;;

let () =
  printf "Creating word set…\n%!";
  let dict = gen_dict () in
  printf "Done\n";
  find_anagram dict Caml.Sys.argv.(1)
;;
