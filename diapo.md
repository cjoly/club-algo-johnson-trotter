% Algorithme de Johnson-Trotter
% C. Joly
% Octobre 2017

# Intro

## Anagrammes

 + aube, beau
 + ironique, onirique

-----

### Approche naïve

 + Récursion
 + n!
 + Générer 1 à 1

# Johnson-Trotter

----------

### Histoire

 + Deux découvertes indépendantes
 + début des années 60
 + 4 cloches : 1620

<a data-flickr-embed="true" data-header="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/41554379@N03/4624061728/in/photolist-83Bwsd-4C5GSk-5yHzkj-6qjSkd-22F3CY-59RBGr-2ZhokM-8h4mVx-a6UAvC-91iZXQ-onticC-hhjSBg-5kVNxg-aYT3DD-nMXDM6-ais3rP-8Xmnx3-4KnTTB-HH935-obTuTN-d1kQff-4yZDqB-6bAZFJ-2E3yk6-4uiCy-3cpj4P-7PKWy-8MA13h-8sbcnw-5zzs8R-77AsYf-7hsXTy-ikfFb-68po-iJSat-2EE1pP-m6eM-4nG8Fa-t5GsP-pDfUs-89a15m-Cd3Jd-VRNrDf-TsJBU-5kTCsU-4TMgw4-8w5xAM-mfQAsT-XKPpoq-4k6LDD" title="Bells"><img src="https://farm4.staticflickr.com/3341/4624061728_25e54b473f_o.jpg" width="455" height="190" alt="Bells"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

----------

## Algo

------------

### Préparer le tableau

----------

#### Tri (ou numérotation)

---- ----- -----
 1      2    3
 e1    e2   e3
---- ----- -----

------

#### Gauche-droite

---- ----- -----
 1      2    3
 e1    e2   e3
 L      L   L
---- ----- -----

-------

### Générer une permutation

#### Trouver le plus grand élément « déplaçable »

Déplaçable : Élément adjacent (gauche ou droite, selon) inférieur a l’élément considéré

#### Déplacer

Échanger les deux éléments

#### Màj L ou R

Mettre L à la place de R (et réciproquement) pour tout élément plus grand que l’élément échangé

----------

### Sur un exemple

1 2 3

1 3 2

3 1 2

3 2 1

2 3 1

2 1 3

-------

# Ressources

## [Inspiration (OCaml)](http://typeocaml.com/2015/05/05/permutation/)

## [Wikipédia](https://en.wikipedia.org/wiki/Steinhaus%E2%80%93Johnson%E2%80%93Trotter_algorithm)
